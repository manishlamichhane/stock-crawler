import json
import os
import subprocess

from flask import Flask, render_template, redirect, url_for
from settings import output_file

app = Flask(__name__)


@app.route('/')
def index():
    with open(output_file) as data_file:
        data = json.load(data_file)
        return render_template('main.html', data=data)


@app.route('/refresh')
def refresh():
    if os.path.exists(output_file):
        os.remove(output_file)

    subprocess.check_output(['scrapy', 'runspider', 'company_detail.py', '-o', 'details.json', '-t', 'json'])

    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

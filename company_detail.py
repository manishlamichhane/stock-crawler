import scrapy

from settings import symbols, target_columns


class StockSpider(scrapy.Spider):
    name = 'stock-spider'
    
    start_urls = ['https://merolagani.com/CompanyDetail.aspx?symbol=%s' % symbol for symbol in symbols]
    data = {}

    def parse(self, response):
        rows = dict()
        for column in response.xpath('//table[@id="accordion"]/tbody'):
            row = {'column_name': column.xpath('tr/th/text()').get()}
            
            if column.xpath('tr/td/strong/span/text()').get():
                row['value'] = column.xpath('tr/td/strong/span/text()').get()

            elif column.xpath('tr/td/span/text()').get() and column.xpath('tr/td/text()').get():
                value_outer = column.xpath('tr/td/text()').get()
                value_inner = column.xpath('tr/td/span/text()').get()
                row['value'] = f'{value_outer} {value_inner}'
            
            elif column.xpath('tr/td/span/text()').get():
                row['value'] = column.xpath('tr/td/span/text()').get()
            
            else:
                row['value'] = column.xpath('tr/td/text()').get()

            row['column_name'] = row['column_name'].replace('\r\n', '').strip()
            row['value'] = row['value'].replace('\r\n', '').strip()
            
            rows[row['column_name']] = row['value']

        # filter out unnecessary columns
        rows = self.filter_columns(rows)

        # Add Company Name and URL
        rows['company_name'] = response.xpath('//h4[@class="page-header"]/span/text()').get()
        rows['company_url'] = response.url

        yield rows

    def filter_columns(self, data):
        return {item: data[item] for item in data.keys() if item in target_columns}
